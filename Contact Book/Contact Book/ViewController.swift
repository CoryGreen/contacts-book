//
//  ViewController.swift
//  Contact Book
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

enum ContactsError: Error {
    case emptyNameField
    case emptyNumberField
}
class ContactTableViewCell: UITableViewCell {
    @IBOutlet weak var phoneImageView: UIImageView!
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    var model: ContactsModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        model = ContactsModel()
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func insertContact(_ sender: UIButton) {
        do {
            let contactName = try setName()
            let numberEntered = numberTextField.text ?? ""
            let newContact = Contact(fullName: contactName, phoneNumber: numberEntered)
            model.appendRow(newContact)
            nameTextField.text = ""
            numberTextField.text = ""
            tableView.reloadData()
        } catch (ContactsError.emptyNameField) {
            showNameAlert()
        } catch {
            print("Yeah, you messed up")
        }
    }
    func setName() throws -> String {
        guard let nameEntered = nameTextField.text?.trimmingCharacters(in: .whitespaces),
            !nameEntered.isEmpty else {
            throw ContactsError.emptyNameField
        }
        return nameEntered.capitalized
    }
    func showNameAlert() {
        let alertController = UIAlertController(title: "Invalid Entry", message: "You cannot enter a contact without a name", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showNumberAlert() {
        let alertController = UIAlertController(title: "No number", message: "The contact does not have a phone number", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showOptions(_ indexPath: IndexPath) throws {
        let contactToCall = model.contact(at: indexPath)
        guard let contactName = contactToCall?.fullName,
            let contactNumber = contactToCall?.phoneNumber,
            !contactNumber.isEmpty
        else {
                throw ContactsError.emptyNumberField
        }
        
        let optionsController = UIAlertController(title: "Call them, maybe", message: "Hey \(contactName) just met you and here's their number \(contactNumber) so call them maybe?", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "How About No", style: .cancel) { action in
            print("Pressed Cancel")
        }
        optionsController.addAction(cancelAction)
        let phoneAction = UIAlertAction(title: "Via Phone", style: .default) { action in print("Chose to call phone")
        }
        optionsController.addAction(phoneAction)
        let skypeAction = UIAlertAction(title: "Via Skype", style: .default) { action in
            print("Chose to skype")
        }
        optionsController.addAction(skypeAction)
        let whatsappAction = UIAlertAction(title: "Via What's App", style: .default) { action in
            print("Chose What's App")
        }
        optionsController.addAction(whatsappAction)
        present(optionsController, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDelegate {}
extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.numberOfSections
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let defaultCell = tableView.dequeueReusableCell(withIdentifier: "contact_id", for: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "contact_id", for: indexPath) as? ContactTableViewCell else { return defaultCell }
        guard let contactDetail = model.contact(at: indexPath) else { return cell }
        cell.textLabel?.text = contactDetail.fullName
        cell.detailTextLabel?.text = contactDetail.phoneNumber
        cell.phoneImageView.isHidden = cell.detailTextLabel?.text?.isEmpty ?? true ? true : false
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return model.sections.isEmpty ? nil : String(model.sections[section]) 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        do {
            try showOptions(indexPath)
        } catch (ContactsError.emptyNumberField) {
            showNumberAlert()
        } catch {
            print("You shouldn't be here")
        }
    }
}
