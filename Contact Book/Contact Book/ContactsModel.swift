//
//  ContactsModel.swift
//  Contact Book
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
//import Foundation

struct Contact {
    var fullName: String
    var phoneNumber: String?
}

class ContactsModel {
    private var contacts: [Character: [Contact]]
    init() {
        contacts = [:]
    }
}

extension Contact: Equatable {
    static func == (contact1: Contact, contact2: Contact) -> Bool {
        return contact1.fullName == contact2.fullName && contact1.phoneNumber == contact2.phoneNumber
    }
}

extension ContactsModel {
    var sections: [Character] {
        return contacts.keys.sorted()
    }
    
    var numberOfSections: Int {
        return sections.count
    }

    func appendRow(_ contact: Contact) {
        guard let key = contact.fullName.first else {
            print("The name field is empty")
            return
        }
        for sectionOfContacts in contacts.values {
            if sectionOfContacts.contains(contact) {
                print("Contact already present in list")
                return
            }
        }
        if listContainsIndex(key) {
            contacts[key]?.append(contact)
            contacts[key]?.sort(by: {$0.fullName > $1.fullName})
        } else {
            let newContactSection = [contact]
            contacts[key] = newContactSection
        }
//        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Contact.plist")
//        print(path)
//        do {
//            let data = encodeContact(contact)
//            try data?.write(to: path)
//        } catch {
//            print(error)
//        }
        
    }
    
    func listContainsIndex(_ firstLetter: Character) -> Bool {
        return contacts.keys.contains(firstLetter)
    }
    
    func numberOfRows(in section: Int) -> Int {
        guard section < sections.count else {
            return 0
        }
        let key = sections[section]
        guard let sectionContacts = contacts[key] else { return 0 }
        return sectionContacts.count
    }
    
    func contact(at indexPath: IndexPath) -> Contact? {
        guard indexPath.section < sections.count else { return nil }
        let key = sections[indexPath.section]
        guard let sectionContacts = contacts[key], indexPath.row < sectionContacts.count else { return nil }
        return sectionContacts[indexPath.row]
    }
}

extension Contact: CustomStringConvertible {
    var description: String {
        return "\(fullName), \(phoneNumber ?? "No number listed")"
    }
}

extension ContactsModel: CustomStringConvertible {
    var description: String {
        return String(describing: contacts.values)
    }
}

//extension ContactsModel {
//    func encodeContact (_ contact: Contact) -> Data? {
//        let encoder = PropertyListEncoder()
//        encoder.outputFormat = PropertyListSerialization.PropertyListFormat.xml
//        do {
//            return try encoder.encode(contact)
//        } catch {
//            print("Encoder error: \(error)")
//            return nil
//        }
//    }
//
//    func decodeContact (_ data: Data) -> Contact? {
//        let decoder = PropertyListDecoder()
//        var contact: Contact? = nil
//
//        do {
//            contact = try decoder.decode(Contact.self, from: data)
//         } catch {
//            print(error)
//         }
//        return contact
//    }
//}
